package org.marsl.tools.epr.library;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.eclipse.compare.IModificationDate;
import org.eclipse.compare.IStreamContentAccessor;
import org.eclipse.compare.ITypedElement;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.graphics.Image;


/**
 * This class represents a compare item needed by the CompareEditorInput DiffNode.
 * 
 * @author Mars
 *
 */
public class CompareItem implements IStreamContentAccessor, ITypedElement,
		IModificationDate {
	
	private String contents;
	private String name;
	
	/**
	 * Constructor.
	 * 
	 * @param Name of the editor frame.
	 * @param Text inside the editor frame.
	 */
	public CompareItem(String name, String contents) {
		this.contents = contents;
		this.name = name;
	}

	/**
	 * Modification date of the compare item.
	 * @return Always returns 0.
	 */
	public long getModificationDate() {
		return 0;
	}

	/**
	 * Name-Getter.
	 * @return Returns the name of the editor frame.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Image-Getter.
	 * @return Always returns null.
	 */
	public Image getImage() {
		return null;
	}

	/**
	 * Type-Getter.
	 * @return Always returns "txt".
	 */
	public String getType() {
		return ITypedElement.TEXT_TYPE;
	}

	/**
	 * Content-Getter.
	 * @return Returns the content as an InputStream.
	 */
	public InputStream getContents() throws CoreException {
		return new ByteArrayInputStream(this.contents.getBytes());
	}
	
	/**
	 * Content-Getter.
	 * @return Returns the content as a String.
	 */
	public String getString() {
		return this.contents;
	}

}
