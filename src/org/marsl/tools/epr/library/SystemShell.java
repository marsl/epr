package org.marsl.tools.epr.library;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;

import org.eclipse.jface.preference.IPreferenceStore;
import org.marsl.tools.epr.Activator;
import org.marsl.tools.epr.preferences.PreferenceConstants;

/**
 * This class does the system shell commands.
 * @author Marcel Linke
 *
 */
public class SystemShell {
	
	private String phpPath;
	private String tmpLibPath;
	
	/**
	 * Constructor
	 * Sets the path to the PHP executable.
	 * Copies the refactor.phar to the tmp folder and sets the path to it.
	 */
	public SystemShell() {
		this.phpPath = getPHPPath();
		this.tmpLibPath = copyRefactoringLibToTmp();
	}

	/**
	 * Executes the refactoring command.
	 * @param Command of the Refactoring library
	 * @return Patchfile as a String.
	 * {@link https://github.com/QafooLabs/php-refactoring-browser}
	 */
	public String executeCommand (String command) {
		
		StringBuffer output = new StringBuffer();
		
		Process p;
		
		try {
			p = Runtime.getRuntime().exec(this.phpPath + " " + this.tmpLibPath + " " + command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			
			String line = "";
			
			int i = 0;
			
			while ((line = reader.readLine())!=null) {
				if (i > 1) {
					output.append(line + "\n");
				}
				i++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return output.toString();
		
	}
	
	/**
	 * Patches a given text with the Patchtext.
	 * @param Patchtext
	 * @param Text which should be patched.
	 * @return Patched text.
	 */
	public String getPatchedText(String executionResult, String text) {
		
		String result = new String();
		
		BufferedReader readExecutionResult = new BufferedReader(new StringReader(executionResult));
		String executionResultLine = null;
		
		BufferedReader readText = new BufferedReader(new StringReader(text));
		String textLine = null;
		
		StringWriter stringWriter = new StringWriter();
		BufferedWriter writeResult = new BufferedWriter(stringWriter);
		
		int diff = 0;
		int curLine = 0;
		int startNext = 1;
		int diffNew = 0;
		
		boolean loop = false;
		
		try {
			while ((textLine = readText.readLine()) != null) {
				curLine++;
				if ((curLine - diff) == startNext) {
					loop = true;
					while (loop) {
						executionResultLine = readExecutionResult.readLine();
						if (executionResultLine == null) {
							writeResult.write(textLine);
							writeResult.newLine();
							loop = false;
						}
						else if (executionResultLine.startsWith("@@")) {
							diff = diffNew;
							String ranges = executionResultLine.substring(3, executionResultLine.length()-3);
							String[] rangesArray = ranges.split(" ");
							
							String[] oldRange = rangesArray[0].substring(1).split(",");
							String[] newRange = rangesArray[1].substring(1).split(",");
							startNext = Integer.parseInt(oldRange[0]);
							diffNew = Integer.parseInt(oldRange[0]) - Integer.parseInt(newRange[0]);
							writeResult.write(textLine);
							writeResult.newLine();
							loop = false;
						}
						else if (executionResultLine.startsWith("-")) {
							startNext++;
							loop = false;
						}
						else if (executionResultLine.startsWith("+")) {
							writeResult.write(executionResultLine.substring(1));
							writeResult.newLine();
						}
						else {
							writeResult.write(textLine);
							writeResult.newLine();
							startNext++;
							loop = false;
						}
					}
				}
				else {
					writeResult.write(textLine);
					writeResult.newLine();
				}
			}
			
			readExecutionResult.close();
			readText.close();
			writeResult.close();
			
		} catch (IOException e) {
			result = text;
		}
		
		result = stringWriter.toString();
		
		return result;
	}
	
	/**
	 * Copies the refactoring library to the Java temporary directory.
	 * @return Path to the refactoring library.
	 */
	private String copyRefactoringLibToTmp() {
		
		String path = System.getProperty("java.io.tmpdir");
		
		File dir = new File (path + File.separator + "epr");
		dir.mkdirs();
		
		InputStream inputStream = getClass().getResourceAsStream("/refactor.phar");
		
		BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
		
		try {
			File.createTempFile("refactor", ".phar", dir.getCanonicalFile());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		File tmpFile = new File(dir + File.separator, "refactor.phar");
		tmpFile.deleteOnExit();
		
		try {
			FileOutputStream outputStream = new FileOutputStream(tmpFile);
			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
			int i;
			while ((i = bufferedInputStream.read()) != -1) {
				bufferedOutputStream.write(i);
			}
			bufferedOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return tmpFile.getAbsoluteFile().toString();
		
	}
	
	/**
	 * Returns the stored path to the PHP executable.
	 * @return Stored PHP path
	 */
	private String getPHPPath() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		String phpPath = store.getString(PreferenceConstants.P_PATH);
		return phpPath;
	}
}
