package org.marsl.tools.epr.library;

import org.eclipse.jface.dialogs.IInputValidator;

/**
 * Validates the inserted String by its length.
 * @author Marcel Linke
 *
 */
public class LengthValidator implements IInputValidator {

	/**
	 * If the length of the text is 0 there will be an error message.
	 * @return Error Message
	 */
	public String isValid(String newText) {
		int len = newText.length();
		
		String result = null;
		
		if (len<1) {
			result = "You have to input a name!";
		}
		
		return result;
	}
}
