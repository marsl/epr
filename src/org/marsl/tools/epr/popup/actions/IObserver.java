package org.marsl.tools.epr.popup.actions;

/**
 * Interface for the notify-methode of the Observer pattern.
 * @author Marcel Linke
 *
 */
public interface IObserver {
	/**
	 * Notifies the observer for changes relevant for it.
	 * @param Changed text.
	 */
	public void notify(String text);
}
