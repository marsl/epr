package org.marsl.tools.epr.popup.actions;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.CompareUI;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.marsl.tools.epr.library.LengthValidator;
import org.marsl.tools.epr.library.SystemShell;
import org.marsl.tools.epr.windows.CompareInput;

/**
 * This class is responsible for the extract method refactoring.
 * @author Marcel Linke
 *
 */
public class ExtractMethod implements IObjectActionDelegate, IObserver {

	private Shell shell;
	private IWorkbenchPart targetPart;

	/**
	 * Constructor
	 */
	public ExtractMethod() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		this.shell = targetPart.getSite().getShell();
		this.targetPart = targetPart;
	}

	/**
	 * Controls the flow for the extract method refactoring.
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		String methodName = "";

		InputDialog inputDialog = new InputDialog(this.shell, "Extract Method",
				"Please enter the name of the new method", "",
				new LengthValidator());

		if (inputDialog.open() == Window.OK) {
			methodName = inputDialog.getValue();
		}

		if (methodName.length() > 0) {

			String executionResult = extractMethod(methodName);

			String text = getText();
			
			SystemShell shell = new SystemShell();

			String newText = shell.getPatchedText(executionResult, text);

			CompareConfiguration compareConfig = new CompareConfiguration();
			CompareUI.openCompareDialog(new CompareInput(text, newText,
					compareConfig, this));
		}

	}

	/**
	 * Builds the command for the extract method command on the shell.
	 * @param Name of the introduced method.
	 * @return Result of the extract method refactoring as a Patchtext.
	 */
	private String extractMethod(String methodName) {
		int start = getStartLine() + 1;
		int end = getEndLine() + 1;

		String fileLocation = getFileLocation();

		String executionCommand = "extract-method " + fileLocation + " "
				+ start + "-" + end + " " + methodName;
		SystemShell shell = new SystemShell();

		String executionResult = shell.executeCommand(executionCommand);
		return executionResult;
	}

	/**
	 * Searchs the path to the currently opened file in the Eclipse editor.
	 * @return Path to the currently opened file
	 */
	private String getFileLocation() {
		
		String result = null;
		
		if (this.targetPart instanceof IEditorPart) {
			if (((IEditorPart) this.targetPart).getEditorInput() instanceof IFileEditorInput) {
				IFile file = ((IFileEditorInput) ((IEditorPart) this.targetPart)
						.getEditorInput()).getFile();
				result = file.getLocation().toOSString();
			}
		}
		return result;
	}

	/**
	 * Returns the complete text of the currently opened file in the Eclipse editor.
	 * @return Text of the currently opened file
	 */
	private String getText() {
		
		IEditorPart activeEditor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		
		String result = null;
		
		if (activeEditor != null) {
			IDocument doc = (IDocument) activeEditor.getAdapter(IDocument.class);
			if (doc != null) {
				result = doc.get();
			}
		}

		return result;
	}
	
	/**
	 * Sets the text in the currently active window in Eclipse editor.
	 * @param Text to set in the currently active window
	 */
	private void setText(String text) {
		IEditorPart activeEditor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		
		if (activeEditor != null) {
			IDocument doc = (IDocument) activeEditor.getAdapter(IDocument.class);
			if (doc != null) {
				doc.set(text);
			}
		}
	}

	/**
	 * Gets the line number of the first marked line in Eclipse editor.
	 * @return Line number, returns -1 if nothing selected
	 */
	private int getStartLine() {
		ITextSelection textSelection = getTextSelection();
		
		int result = -1;
		
		if (textSelection != null) {
			result = textSelection.getStartLine();
		}
		return result;
	}

	/**
	 * Gets the line number of the last marked line in Eclipse editor.
	 * @return Line number, returns -1 if nothing selected
	 */
	private int getEndLine() {
		ITextSelection textSelection = getTextSelection();
		
		int result = -1;
		
		if (textSelection != null) {
			result = textSelection.getEndLine();
		}
		return result;
	}

	/**
	 * Returns the current TextSelection.
	 * @return ITextSelection
	 */
	private ITextSelection getTextSelection() {
		ISelection selection = this.targetPart.getSite().getSelectionProvider()
				.getSelection();
		
		ITextSelection result = null;
		
		if (selection instanceof ITextSelection) {
			result = (ITextSelection) selection;
		}

		return result;
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	/**
	 * Notifies the observer for changes relevant for it.
	 * @param Changed text.
	 */
	public void notify(String text) {
		setText(text);
	}

}
