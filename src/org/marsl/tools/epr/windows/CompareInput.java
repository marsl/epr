package org.marsl.tools.epr.windows;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.CompareEditorInput;
import org.eclipse.compare.CompareUI;
import org.eclipse.compare.ITypedElement;
import org.eclipse.compare.structuremergeviewer.DiffNode;
import org.eclipse.compare.structuremergeviewer.Differencer;
import org.eclipse.compare.structuremergeviewer.ICompareInput;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.marsl.tools.epr.library.CompareItem;
import org.marsl.tools.epr.popup.actions.IObserver;

/**
 * A compare viewer which is extending the behaviour of the CompareEditorInput
 * 
 * @author Marcel Linke
 * @see CompareEditorInput
 *
 */
public class CompareInput extends CompareEditorInput {

	private String beforeText;
	private String newText;
	private IObserver RefactoringClass;

	/**
	 * Constructor of the CompareInput class.
	 * 
	 * @param Text before the refactoring
	 * @param Text after the refactoring
	 * @param CompareConfiguration
	 * @param Class which should be informed when pressed ok.
	 */
	public CompareInput(String beforeText, String newText,
			CompareConfiguration compareConfig, IObserver RefactoringClass) {
		super(compareConfig);
		this.beforeText = beforeText;
		this.newText = newText;
		this.setDirty(true);
		this.RefactoringClass = RefactoringClass;
	}

	/**
	 * Prepares the input for both sides of the compare view.
	 * @param IProgressMonitor
	 * @return Returns a DiffNode.
	 */
	protected Object prepareInput(IProgressMonitor monitor)
			throws InvocationTargetException, InterruptedException {
		CompareItem beforeItem = new CompareItem("Before", this.beforeText);
		CompareItem newItem = new CompareItem("New", this.newText);
		return new DiffNode(null, Differencer.CHANGE, null, beforeItem, newItem);
	}

	/**
	 * Labels the OK button.
	 * @return Returns the text "Finish".
	 */
	@Override
	public String getOKButtonLabel() {
		return "Finish";
	}

	/**
	 * Logic which sould be executed when OK is pressed.
	 * @return true, when the operation was successful
	 */
	@Override
	public boolean okPressed() {
		ICompareInput compareInput = (ICompareInput) this.getCompareResult();
		ITypedElement typedElement = compareInput.getRight();
		if (typedElement != null) {
			IDocument doc = CompareUI.getDocument(typedElement);
			if (doc != null) {
				this.RefactoringClass.notify(doc.get());
			}
		}
		return true;
	}

}
